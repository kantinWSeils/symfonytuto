<?php

namespace App\Entity;

use App\Repository\CategoryBlogPostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryBlogPostRepository::class)
 */
class CategoryBlogPost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name = null;

    /**
     * @ORM\OneToMany(targetEntity=BlogPost::class, mappedBy="category")
     */
    private $blogPosts;

    public function __construct()
    {
        $this->blogPosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, BlogPost>
     */
    public function getBlogPosts(): Collection
    {
        return $this->blogPosts;
    }

    public function addBlogPosts(BlogPost $blogPosts): self
    {
        if (!$this->blogPosts->contains($blogPosts)) {
            $this->blogPosts[] = $blogPosts;
            $blogPosts->setCategory($this);
        }

        return $this;
    }

    public function removeBlogPosts(BlogPost $blogPosts): self
    {
        if ($this->blogPosts->removeElement($blogPosts)) {
            // set the owning side to null (unless already changed)
            if ($blogPosts->getCategory() === $this) {
                $blogPosts->setCategory(null);
            }
        }

        return $this;
    }
}
